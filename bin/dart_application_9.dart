import 'dart:io';

void main() {
  int sum = 0;
// Category
  print("โปรดเลือกหมวดหมู่ที่ต้องการ : ");
  print("1 : กาแฟ \n2 : ชา \n3 : นม");
  var ipcategory = stdin.readLineSync();

// Coffee
  if (ipcategory == '1') {
    print("หมวดหมู่กาแฟ : โปรดเลือกเมนูที่ต้องการ");
    print(
        "1 : มอคค่าเย็น  35 บาท \n2 : กาแฟดำร้อน  30 บาท \n3 : ลาเต้เย็น  35 บาท ");
    var ipcoffee = stdin.readLineSync();
    var cof = coffee();
    cof.c = ipcoffee;
    String orderCof = cof.orderCof(cof.c);

// Price of Coffee
    var cofprice = coffeePrice();
    cofprice.cp = ipcategory;
    String cPrice = cofprice.cPrice(cofprice.cp);
    var cofp = int.parse(cPrice);
    sum = sum + cofp;
  }

// Cha
  else if (ipcategory == '2') {
    print("หมวดหมู่ชา : โปรดเลือกเมนูที่ต้องการ");
    print(
        "1 : ชาเขียวเย็น  35 บาท \n2 : ชาร้อน  30 บาท \n3 : ชาไทยเย็น  35 บาท ");
    var ipmenu = stdin.readLineSync();
    var menu = cha();
    menu.c = ipmenu;
    String order = menu.orderCha(menu.c);

// Price of Cha
    var chaaprice = chaPrice();
    chaaprice.cp = ipcategory;
    String cPrice = chaaprice.cPrice(chaaprice.cp);
    var chap = int.parse(cPrice);
    sum = sum + chap;
  }

// Milk
  else if (ipcategory == '3') {
    print("หมวดหมู่นม : โปรดเลือกเมนูที่ต้องการ");
    print(
        "1 : นมชมพูเย็น  35 บาท \n2 : โกโก้ร้อน  30 บาท \n3 : โอวัลตินเย็น  35 บาท ");
    var ipmenu = stdin.readLineSync();
    var menu = milk();
    menu.m = ipmenu;
    String order = menu.orderMilk(menu.m);

// Price of Milk
    var milkyprice = milkPrice();
    milkyprice.mp = ipcategory;
    String mPrice = milkyprice.mPrice(milkyprice.mp);
    var mlkp = int.parse(mPrice);
    sum = sum + mlkp;
  }

// Topping
  if (ipcategory == "1" || ipcategory == "2" || ipcategory == "3") {
    print("กรุณาเลือกทอปปิ้งที่ต้องการ !!!");
    print(
        "0 : ไม่เพิ่ม \n1 : ผงโอวัลติน  +5 บาท \n2 : ผงโอริโอ้  +5 บาท \n3 : วิปครีม  +10 บาท ");
    var ipTop = stdin.readLineSync();
    var topp = topping();
    topp.t = ipTop;
    String top = topp.orderTopping(topp.t);

// Price of Topping
    var toppPrice = toppingPrice();
    toppPrice.tp = ipTop;
    String tPrice = toppPrice.topPrice(toppPrice.tp);
    var toppp = int.parse(tPrice);
    sum = sum + toppp;
  }
  if (ipcategory == "1" ||
      ipcategory == "2" ||
      ipcategory == "3" ||
      ipcategory == "4") {
// ฝาและหลอด
    print("หลอด และฝา !!!");
    print("0 : ไม่รับ \n1 : หลอด \n2 : ฝา \n3 : หลอด และฝา ");
    var ipeq = stdin.readLineSync();
    var fhaAndLod = lodAndFha();
    fhaAndLod.laf = ipeq;
    String fnl = fhaAndLod.lodNFha(fhaAndLod.laf);
    print(fnl);

// ชำระเงิน
    print("ราคาเครื่องดื่มของคุณ : $sum บาท");
    print("ช่องทางการชำระเงิน !!!");
    print("1 : เงินสด \n2 : promptpay \n3 : truemoney wallet");
    var ippayment = stdin.readLineSync();
    var paym = payment();
    paym.pm = ippayment;
    String paymoney = paym.pay(paym.pm);
    print(paymoney);
    print("..... \n... \n.");
    print("เต่าบินขอขอบคุณครับ ขอให้ลูกค้าทานเครื่องดื่มให้อร่อยครับ");
  }
}

class coffee {
  var c;

  String orderCof(String c) {
    var chooseCof;

    if (c == '1') {
      print("เมนูที่เลือก : มอคค่าเย็น");
      chooseCof = "มอคค่าเย็น";
    } else if (c == '2') {
      print("เมนูที่เลือก : กาแฟดำร้อน");
      chooseCof = "กาแฟดำร้อน";
    } else if (c == '3') {
      print("เมนูที่เลือก : ลาเต้เย็น");
      chooseCof = "ลาเต้เย็น";
    }
    return chooseCof;
  }
}

class coffeePrice {
  var cp;

  String cPrice(String cp) {
    var Cofprice;

    if (cp == "1") {
      Cofprice = "35";
    } else if (cp == "2") {
      Cofprice = '30';
    } else if (cp == "3") {
      Cofprice = "35";
    }
    return Cofprice;
  }
}

class cha {
  var c;

  String orderCha(String c) {
    var chooseCha;

    if (c == '1') {
      print("เมนูที่เลือก : ชาเขียวเย็น");
      chooseCha = "ชาเขียวเย็น";
    } else if (c == '2') {
      print("เมนูที่เลือก : ชาร้อน");
      chooseCha = "ชาร้อน";
    } else if (c == '3') {
      print("เมนูที่เลือก : ชาไทยเย็น");
      chooseCha = "ชาไทยเย็น";
    }
    return chooseCha;
  }
}

class chaPrice {
  var cp;

  String cPrice(String cp) {
    var chaprice;

    if (cp == "1") {
      chaprice = "35";
    } else if (cp == "2") {
      chaprice = '30';
    } else if (cp == "3") {
      chaprice = "35";
    }
    return chaprice;
  }
}

class milk {
  var m;

  String orderMilk(String m) {
    var chooseMilk;

    if (m == '1') {
      print("เมนูที่เลือก : นมชมพูเย็น");
      chooseMilk = "นมชมพูเย็น";
    } else if (m == '2') {
      print("เมนูที่เลือก : โกโก้ร้อน");
      chooseMilk = "โกโก้ร้อน";
    } else if (m == '3') {
      print("เมนูที่เลือก : โอวัลตินเย็น");
      chooseMilk = "โอวัลตินเย็น";
    }
    return chooseMilk;
  }
}

class milkPrice {
  var mp;

  String mPrice(String mp) {
    var mprice;

    if (mp == "1") {
      mprice = "35";
    } else if (mp == "2") {
      mprice = '30';
    } else if (mp == "3") {
      mprice = "35";
    }
    return mprice;
  }
}

class topping {
  var t;

  String orderTopping(String t) {
    var chooseTopping;
    if (t == "0") {
      print("ทอปปิ้งที่เลือก : ไม่เพิ่ม");
      chooseTopping = "ผงโอวัลติน";
    } else if (t == "1") {
      print("ทอปปิ้งที่เลือก : ผงโอวัลติน");
      chooseTopping = "ผงโอวัลติน";
    } else if (t == "2") {
      print("ทอปปิ้งที่เลือก : ผงโอริโอ้");
      chooseTopping = "ผงโอริโอ้";
    } else if (t == "3") {
      print("ทอปปิ้งที่เลือก : วิปครีม");
      chooseTopping = "วิปครีม";
    }
    return chooseTopping;
  }
}

class toppingPrice {
  var tp;

  String topPrice(String tp) {
    var priceTopping;

    if (tp == "0") {
      priceTopping = "0";
    } else if (tp == "1") {
      priceTopping = "5";
    } else if (tp == "2") {
      priceTopping = "5";
    } else if (tp == "3") {
      priceTopping = "10";
    }
    return priceTopping;
  }
}

class lodAndFha {
  var laf;

  String lodNFha(String laf) {
    var equipment;

    if (laf == "0") {
      equipment = "ไม่รับ";
    } else if (laf == "1") {
      equipment = "หลอด";
    } else if (laf == "2") {
      equipment = "ฝา";
    } else if (laf == "3") {
      equipment = "หลอด และฝา";
    }
    return equipment;
  }
}

class payment {
  var pm;

  String pay(String pm) {
    var paymoney;

    if (pm == "1") {
      paymoney = "เงินสด";
    } else if (pm == "2") {
      paymoney = "QR Promptpay";
    } else if (pm == "3") {
      paymoney = "Qr Truemoney wallet";
    }
    return paymoney;
  }
}
//Finish